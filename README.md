---
filename: README.md
Its Licence: CC BY-SA 4.0
title: README
Author: Néhémie
Aim: Reference when landing
Last modification: 2022-01-01
---

Spatial data represent abstractions of real-world entities.  Some
of them have a complex structure and a massive volume. This is
the case for data acquired through *remote sensing*, defined as:

> *The science (and to some extent, art) of acquiring information
> about the Earth's surface without actually being in contact
> with it. This is done by sensing and recording reflected or
> emitted energy and processing, analyzing, and applying that
> information*. [Canada Centre for Remote Sensing][CCRS-intro]

With the advancement of sensor and computing technologies, an
ever-increasing volume of data is produced and widely made freely
[(see License)](#License) accessible.  Citizen science and
collaborative free geographic databases (such as OpenStreetMap)
provide another layer of extensive spatial information.  While
this offers opportunities for science and society, it poses
challenges for computing and it comes with a rising cost of
energy, time and hardware infrastructure to download, store,
manage, process, and analyse these data.

This repository is a modest personal take. It strives to make my
work reproducible but with a low frequency of repetition. I try
to find a balance between optimisation and my coding capacities
(see [Li *et al.*](#Li2020a) for a wider framework)

## License {#License}

This repository uses diverse and multiple datasets. Specific
licenses are provided within subdirectories.

### Sentinel-2

Sentinel-2 Rasters available here are derivative from the data
provided by the Sentinel-2 Mission of the ESA.

 > SENTINEL-2 is a wide-swath, high-resolution, multi-spectral
 > imaging mission, supporting Copernicus Land Monitoring
 > studies, including the monitoring of vegetation, soil and
 > water cover, as well as observation of inland waterways and
 > coastal areas −
 [ESA] (retrieved 2017-10-23 16:13:32)

While the download of files requires identification, it is
possible to redistribute the data. In some cases, I skipped the
reproduction of the download and I provide here only the modified
version with a link to the original data. Therefore derivative
"\*.tif" files must be used with the mention "Contains modified
Copernicus Sentinel data 20[1-2][0-9]". For more information, see
[Terms of Sentinel Data Hub portals and Data supply conditions
][sentinel_conditions].


## References

[CCRS-intro]: https://www.nrcan.gc.ca/maps-tools-and-publications/satellite-imagery-and-air-photos/tutorial-fundamentals-remote-sensing/introduction/9363

[ESA]: https://sentinel.esa.int/web/sentinel/user-guides/sentinel-2-msi/overview

[sentinel_conditions]: https://scihub.copernicus.eu/twiki/do/view/SciHubWebPortal/TermsConditions

### Li *et alii* 2020 {#Li2020a}

Li Z., Gui Z., Hofer B., Li Y., Scheider S., Shekhar S. (2020)
Geospatial Information Processing Technologies. In: Guo    H.,
Goodchild M.F., Annoni A. (eds) Manual of Digital Earth.
Springer, Singapore.
<https://doi.org/10.1007/978-981-32-9915-3_6>

