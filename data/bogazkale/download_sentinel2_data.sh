#!/usr/bin/env sh
#
# init: 2022-01-02
# directory: pyla-koutsopetria_archaeological_project/
# filename: download_sentinel2_data.sh
# modification: 2022-01-02
#
# overview:
#   download sentinel data
#
# -------------------------------------
data_dir="./data/"
config="./config.yaml"
if [ -e "${config}" ]; then
    qr=$(grep  --only-matching --perl-regexp --color=never "(?<=qr: ).*" "${config}")
    granule=$(grep --only-matching --perl-regexp --color=never "(?<=granule: ).*" "${config}")
    echo "Detail of the granule ${granule} are saved in ${qr}"
fi
# Variables ==================



## Secret variable defined on GitLab/CI settings
## This is for local testing of script
secrets="./secret.yaml"
if [ -e "${secrets}" ]; then
  copernicus_data_hub_pw=$(grep  --only-matching --perl-regexp --color=never "(?<=pw: ).*" "${secrets}")
  copernicus_data_hub_login=$(grep --only-matching --perl-regexp --color=never "(?<=login: ).*" "${secrets}")
fi

if [ ! -d "${data_dir}" ]; then
 mkdir -p "${data_dir}"
fi
# Query    ==============

cd ${data_dir} || cd ..

wget --no-check-certificate \
  --user="${copernicus_data_hub_login}" \
  --password="${copernicus_data_hub_pw}"\
  --output-document="${qr}" \
  "https://scihub.copernicus.eu/dhus/search?q='"${granule}"'"

grep --only-matching --perl-regexp "Size:.*(?=<)" "${qr}"
uuid=$(grep --only-matching --perl-regexp --color=never "(?<=uuid\">).*(?=<)" "${qr}" )

# Download ==============
wget --continue \
  --user="${copernicus_data_hub_login}" \
  --password="${copernicus_data_hub_pw}"\
  --output-document="${granule}.zip" \
  "https://scihub.copernicus.eu/dhus/odata/v1/Products('"$uuid"')/\$value"

