README ``gis-copernicus/pyla-koutsopetria_archaeological_project/``
==========

Only an end-product for mapping is available here. The original
product is
´S2A_MSIL2A_20170802T082601_N0205_R021_T36SWD_20170802T083801´
available for registered users at
https://scihub.copernicus.eu/dhus/odata/v1/Products('bd4e72de-563a-466f-81ec-cbb4e59c5972')/$value

    Date: 2017-08-02T08:26:01.026Z
    Identifier: S2A_MSIL2A_20170802T082601_N0205_R021_T36SWD_20170802T083801
    Instrument: MSI
    Satellite: Sentinel-2
    Size: 1.02 GB

Copyright for \*.tif files
--------------------------

    Contains modified Copernicus Sentinel data 2017

For more information, see [Terms of Sentinel Data Hub portals and Data supply
conditions
](https://scihub.copernicus.eu/twiki/do/view/SciHubWebPortal/TermsConditions)
and the Licence included in LICENSE/
