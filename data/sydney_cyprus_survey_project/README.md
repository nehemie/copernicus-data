README ``gis-copernicus/sydney_cyprus_survey_project/``
==========

Only an end-product for mapping is available here. The original
product is
´S2B_MSIL1C_20171115T083209_N0206_R021_T36SWD_20171115T105638´
available for registered users at
https://scihub.copernicus.eu/dhus/odata/v1/Products('bbb2d036-7500-4cb3-acd1-c254ffe5e854')/$value 

    Date: 2017-11-15T08:32:09.027Z
    Identifier: S2B_MSIL1C_20171115T083209_N0206_R021_T36SWD_20171115T105638
    Instrument: MSI
    Satellite: Sentinel-2

Copyright for \*.tif files
--------------------------

    Contains modified Copernicus Sentinel data 2017

For more information, see [Terms of Sentinel Data Hub portals and Data supply
conditions
](https://scihub.copernicus.eu/twiki/do/view/SciHubWebPortal/TermsConditions)
and the Licence included in LICENSE/
