 # satdata |         2021-12-31 |
 # ============ izmir_srtm.R ====
 #

 # This is a script to provide a background hillshade

folder <- "data/srtm/"
file <- "srtm1arc.tif"

# DEM ========================

library(stars)

## Read file -----------------
srtm <- paste0(folder, file)

### Resample by factor 3
rasterio <- list(nXOff = 1, nYOff = 1,
                nXSize = 7201, nYSize = 3601,
                nBufXSize = 7200/3, nBufYSize = 7200/3,
                bands = 1)
srtm_dem <- read_stars(srtm,
                       RasterIO = rasterio)
## Cleaning ------------------
 # For simplicity, no negative value
srtm_dem[srtm_dem < 0] <- 0

## Polygonize to vector ------
srtm_cont <- st_contour(srtm_dem,
                        breaks = seq(0, 1800, 200))

# Hillshade ==================

 # # Hilshade done  with gdal_merge # #

 # gdaldem \
 # hillshade \
 # -az 270 -alt 40 \
 # srtm1arc.tif \
 # hillshade.tif


## Read file -----------------

file_hill <- "hillshade.tif"
hillshade_f <- paste0(folder, file_hill)

### Resample by factor 6
rasterio <- list(nXOff = 1, nYOff = 1,
                nXSize = 7201, nYSize = 3601,
                nBufXSize = 7200/6, nBufYSize = 3600/6,
                bands = 1)
hillshade <- read_stars(hillshade_f,
                        RasterIO = rasterio)

# For slicing hillshade (only when elevation > 88)
hill_slice <- st_warp(srtm_dem, hillshade)
hill_slice[hill_slice < 88] <- NA
hillshade[is.na(hill_slice)] <- NA

print("")
print("------------------------")
print("Object 'srtm_cont' ready")
print("------------------------")
print("")
print(srtm_cont)
print("------------------------")
print("Object 'hillshade' ready")
print("------------------------")
print(hillshade)
print("")
print("------------------------")
print("Good Luck")
print("------------------------")
print("")
# bye-bye....................|
