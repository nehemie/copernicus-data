Mapping the location of Karabel (Izmir, Turkey) with SRTM and OSM data
======================================================================

Description
------------

Author: Néhémie Strupler

Date: 2022-10-12

Abstract: Code to reproduce the map of the publication: Néhémie
Strupler, "'Vandalising' Father Hittite. Karabel, Orientalism and
Historiographies", Cambridge Archaeological Journal


Procedure
---------

This folder combines script for [R], [sf], [stars], [GDAL] and bash to automatically:

 - download [SRTM] Data and create a DEM with GDAL tools (file "prepare_srtm_data.sh")
 - download [OSM] Data and filter the required features with R (file "izmir_osmdata.R"
 - load the DEM in R via the packages [sf] and [stars] (file "izmir_srtm.R")
 - generate a man with R and the package [tmap] (file: "situate_karabel.R")

Calling the "Makefile" will create the file "Situation_Map_Karabel_YYYY-MM-DD.png".


Requirement
-----------

This script was tested on Debian GNU/Linux bookworm/sid (5.19.0-1-amd64, 2022-09-01)

**N.B. For reproducibility, the SRTM and OSM data are already provided.**

If you want to download the SRTM data, you must change the credentials with your valid NASA Earthdata Credentials
 (https://urs.earthdata.nasa.gov/) in the file "secret.yaml" such as

    ## Secret variable defined on GitLab/CI settings
    ---
    pw: password
    login: username
    ...

If you want to download the OSM data, you must first delete the file "data/izmikar.osm"



References:
===========

[R]: R Core Team (2022). R: A language and environment for statistical computing. R Foundation for Statistical Computing, Vienna, Austria. URL https://www.R-project.org/.

[sf]: Pebesma, E., 2018. Simple Features for R: Standardized Support for Spatial Vector Data. The R Journal 10 (1), 439-446, https://doi.org/10.32614/RJ-2018-009

[stars]: Pebesma E (2022). _stars: Spatiotemporal Arrays, Raster and Vector Data Cubes_. R package version 0.5-6, <https://CRAN.R-project.org/package=stars>.

[tmap]: Tennekes M (2018). “tmap: Thematic Maps in R.” _Journal of Statistical Software_, *84*(6), 1-39.  doi:10.18637/jss.v084.i06 <https://doi.org/10.18637/jss.v084.i06>.

[GDAL]: GDAL/OGR contributors (2022). GDAL/OGR Geospatial Data Abstraction software Library. Open Source Geospatial Foundation. URL https://gdal.org DOI: 10.5281/zenodo.5884351

[SRTM]: NASA JPL (2013). <i>NASA Shuttle Radar Topography Mission Global 1 arc second</i> [Data set]. NASA EOSDIS Land Processes DAAC. Accessed 2022-10-12 from https://doi.org/10.5067/MEaSUREs/SRTM/SRTMGL1.003

[OSM]: OpenStreetMap contributors (https://www.openstreetmap.org, 2022) Planet dump retrieved from https://planet.osm.org


