 # satdata |              2022-01-01 |
 # ============ situate_karabel.R ====
 #
 # This is a script to make a situation map of Karabel. It is a
 # play that brings together data from OSM with SRTM data. Let's
 # see.
 #

basename <- paste0("Situation_Map_Karabel_",
                   format(Sys.Date(), "%Y-%m-%d"))
file_ext <- ".png"
map_filename <- paste0(basename,file_ext)
# source OSM ========================

source("izmir_osmdata.R")
rm("coastline", "curl_overpass", "drop_call", "osm_look", "osmf_call",
   "osmfilter", "overpass")


# Get rasted! ================
source("izmir_srtm.R")



# Start tmap =================
library(grid)
library(tmap)
library(magrittr)
library(colorspace)
tmap_mode("plot")


## Make inset -----------------

### Crop part of the world
library(rnaturalearth)
library(rnaturalearthdata)
world <- ne_countries(scale = "medium", returnclass = "sf")
world <- st_make_valid(st_transform(world, 4326))
world <- world[st_is_valid(world), "sovereignt"]
### Crop Turkey to highlight country
turkey <- world[world$sovereignt=="Turkey",]
### Crop Turkey with Greece to highlight Map in Inset Box 
turkey_and_greece <- st_union(world[world$sovereignt %in%
                              c("Turkey", "Greece"),])

### Fix variable of maps and inset
inset_xlim <- c(20, 60)
inset_ylim <- c(23, 47)

#### Inset_box slightly wider for cropping larger
inset_box <- st_as_sfc(
               st_bbox(c(xmin = inset_xlim[1] - 1,
                         xmax = inset_xlim[2] + 1,
                         ymin = inset_ylim[1] - 2.5,
                         ymax = inset_ylim[2] + 0.5),
                         crs = st_crs(4326))
                      )

#### Define Map section inside inset
map_xlim <- c(26.20, 27.8)  #(bigger than for better visibility)
map_ylim <- c(38.01, 38.78)

map_box <- st_as_sfc(
               st_bbox(c(xmin = map_xlim[1] - 2,
                         xmax = map_xlim[2] + 2,
                         ymin = map_ylim[1] - 1.0,
                         ymax = map_ylim[2] + 1.0),
                         crs = st_crs(4326))
                      )
### Make shapefile for inset
turkey_cropped <- st_intersection(turkey_and_greece, map_box)
aegean_sea_cropped <- st_make_valid(st_difference(map_box,
                                                  turkey_cropped)[2])
world_cropped <- st_crop(world, inset_box)
world_cropped <- world_cropped %>%
                        st_union(.) %>%
                        st_cast(.,group=TRUE) %>%
                        st_make_valid(.)
world_sea_cropped <- st_make_valid(st_difference(inset_box,
                                                 world_cropped)[2])


# Colors ======================
my_terrain_colors_def <- c("#C2D4CB", "#FFD2C0", "#FCFEB7")

my_terrain_colors <- colorRampPalette(my_terrain_colors_def)

# Inset ======================

inset_karabel <-
  # World --------------------
  tm_shape(world_sea_cropped,
           xlim = inset_xlim,
           ylim = inset_ylim) +
    tm_fill(col = "#6bc6fb" ,
            alpha = 2/3) +
    tm_borders(col = "#00000080",
               lwd = 0.5)+
  tm_shape(world) +
    tm_fill(col = grey(2/3) ,
            alpha = 5/6) +
    tm_borders(col = "#00000080",
               lwd = 0.5)+
  # Turkey -------------------
 tm_shape(turkey) +
    tm_fill(col = my_terrain_colors_def[3]) +
    tm_borders(col = "#000000",lwd = 0.8) +
  # Frame --------------------
 tm_shape(turkey_cropped) +
    tm_fill(col = my_terrain_colors_def[2], alpha=2/3) +
    tm_borders(lwd = 0.5, col="black") +
 tm_shape(aegean_sea_cropped) +
    tm_fill(col = "white",
            alpha = 2/3) +
    tm_borders(lwd = 0.5, col="black") +
 tm_shape(map_box) +
    tm_borders(lwd = 2.0, col = "#da6670") +
  # Layout -------------------
 tm_layout(bg.color = "#ffffff",
           frame = "#000000",
           frame.lwd = 2,
           frame.double.line = FALSE,
           outer.margins = c(rep(0.01, 4)),
           inner.margins = c(rep(0.01, 4)))




# Plotting ===================
{
# Hillshade
tm_shape(hillshade,
         xlim = map_xlim,
         ylim = map_ylim ) +
  tm_raster(alpha = 0.9,
            palette = grey(1:(10 - 1)/10),
            style = "quantile",
            legend.show = FALSE) +
# Dem as vectorfile ----------
tm_shape(srtm_cont[,1]) +
  tm_sf(alpha=0.7,
        col=basename(srtm),
        border.col="white",
        border.lwd=0.01,
        border.alpha=0.1,
        n = 8,
        palette=my_terrain_colors(8),
        legend.show = FALSE,
        title = "Elevation",
             legend.is.portrait = FALSE

        ) +

 # Context vectors -----------
tm_shape(buildings[,]) +
  tm_sf(col=grey(2/5), alpha = 0.10, size=0.005) +
tm_shape(waterway) +
	tm_lines("#0571b0") +
tm_shape(highway_s) +
	tm_lines("black", lwd=1.2, alpha=0.9) +
tm_shape(aegean_sea[1]) + tm_borders(col="black") +
	tm_fill("#0571b0", alpha = 0.6) +

 # Name of Places ------------
tm_shape(place[place$name %in% "İzmir",]) +
  tm_text("name", col="white", size=1.0,
          shadow = TRUE, fontface = "bold",
          xmod = -1.5, ymod = 0.7)+
tm_shape(place[place$name %in% c("Kemalpaşa"),]) +
  tm_text("name",
          col="black", size=0.9,
#          col="white", size=1.0,
          bg.color=my_terrain_colors_def[6], bg.alpha=0.8,
          shadow = TRUE, fontface = "bold",
          xmod=-0.1, ymod=2.2)+
tm_shape(place[place$name %in% c("Torbalı"),]) +
  tm_text("name",
          col="black", size=0.9,
#          col="white", size=1.0,
          shadow = TRUE, fontface = "bold",
          bg.color=my_terrain_colors_def[6], bg.alpha=0.8,
          xmod=1.9, ymod=1.5)+
tm_shape(karabel) +
	tm_dots(size=0.5) +
  tm_text("name", col="black",
          shadow = TRUE, fontface = "bold",
          xmod=2.0, ymod=0.5)+

 # Layout --------------------
tm_scale_bar(position = c("left", "bottom")) +
tm_layout(legend.width = 1/3,
          bg.color="lightcyan1",
	outer.margins=rep(0.0, 4),
	inner.margins=rep(0.0, 4)) -> tmapsave

#tmapsave

tmap_save(tmapsave,
          insets_tm = inset_karabel,
          insets_vp = viewport(x = 0.91, y = 0.89,
                               width = 0.2, height = 0.2),
          filename = map_filename)
 }
writeLines(text=map_filename, con="path_to_results.txt")
NULL

# # # bye-bye....................|
