#!/usr/bin/env sh

# -------------------------------------
# wd: izmir_karabel
# date: 2021-12-31
# filename: prepare_srtm_data.sh
#
# Aim:
#  - Download 2 SRTM files from Izmir region
#  - Merge
#  - Hillshade
# -------------------------------------

# Variables ==================

SRTM_NAME_1="N38E026"
SRTM_NAME_2="N38E027"
DATA_DIR="./data/srtm/"

## Secret variable defined on GitLab/CI settings
# NASA_Earthdata_Login_User=****
# NASA_Earthdata_Login_pw=****
secrets="./secret.yaml"
if [ -e "${secrets}" ]; then
  NASA_Earthdata_Login_pw=$(grep \
  --only-matching --perl-regexp --color=never \
                          "(?<=pw: ).*" "${secrets}")
  NASA_Earthdata_Login_User=$(grep \
    --only-matching --perl-regexp --color=never \
                          "(?<=login: ).*" "${secrets}")
fi

# Download ==============
wget -r \
   --user="$NASA_Earthdata_Login_User" \
   --password="$NASA_Earthdata_Login_pw" \
   http://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/2000.02.11/"$SRTM_NAME_1".SRTMGL1.hgt.zip \
   --output-document="$SRTM_NAME_1.zip"

wget -r \
   --user="$NASA_Earthdata_Login_User" \
   --password="$NASA_Earthdata_Login_pw" \
   http://e4ftl01.cr.usgs.gov/MEASURES/SRTMGL1.003/2000.02.11/"$SRTM_NAME_2".SRTMGL1.hgt.zip \
   --output-document="$SRTM_NAME_2.zip"


# Unzip ==================
mkdir -p "$DATA_DIR"
unzip -d "$DATA_DIR" $SRTM_NAME_1
unzip -d "$DATA_DIR" $SRTM_NAME_2

# Merge ==================
cd $DATA_DIR || echo "cd $DATA_DIR failed"
gdal_translate $SRTM_NAME_1.hgt $SRTM_NAME_1.tif
gdal_translate $SRTM_NAME_2.hgt $SRTM_NAME_2.tif
gdal_merge.py \
 "$SRTM_NAME_1.tif" \
 "$SRTM_NAME_2.tif" \
  -o srtm1arc.tif

# Hillshade ==============
gdaldem hillshade \
 -az 270 -alt 40 \
 srtm1arc.tif \
 hillshade.tif

echo "--- end of `basename $0` ---"
