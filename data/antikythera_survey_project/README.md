---
init: 2022-01-01
directory: antikythera_survey_project/
filename: README.md
modification: 2022-01-01

overview:
  brief license declaration
...

# README

Only an end-product for mapping is available here. The original
product is
´S2A_MSIL2A_20170913T091021_N0205_R050_T34SGE_20170913T091832´
available for registered users at
<https://scihub.copernicus.eu/dhus/odata/v1/Products('bab6ae09-a2ca-4306-8b87-3b97039d8539')/$value>


Copyright for \*.tif files
--------------------------

    Contains modified Copernicus Sentinel data 2017

For more information, see [Terms of Sentinel Data Hub portals and Data supply
conditions
](https://scihub.copernicus.eu/twiki/do/view/SciHubWebPortal/TermsConditions)

